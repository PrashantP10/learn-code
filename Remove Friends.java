import java.util.*;
import java.io.*;
 
class RemoveFriends {
    BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
    int numberOfFriends, numberOfFriendsDeleted;
    LinkedList<Integer> friendList = new LinkedList<Integer>(); 
    
    
    public void startRemovalProcess(int totalTestCases) throws Exception{
        
        for (int testCaseCount = 0; testCaseCount < totalTestCases; testCaseCount++) {
            getUserInput();
            
            String[] friendsPopularityArray = getFriendListWithTheirPopularity();
            removeFriend(friendsPopularityArray);
            displayRemainingFriends();
            friendList.clear();
        }
        
    }
    
    public void getUserInput() throws Exception{
        String inputStreamFor_numberOfFriends_numberOfFriendsToDelete = bufferReader.readLine();
        numberOfFriends = Integer.parseInt(inputStreamFor_numberOfFriends_numberOfFriendsToDelete.split(" ")[0]);
        numberOfFriendsDeleted = Integer.parseInt(inputStreamFor_numberOfFriends_numberOfFriendsToDelete.split(" ")[1]);
    }
    
    
    
    public String[] getFriendListWithTheirPopularity() throws Exception {
        String[] friendsPopularityArray = bufferReader.readLine().split(" ");
        return friendsPopularityArray;
    }
    
    public void removeFriend(String[] friendsPopularityArray){
		for(int i = 0 ; i < numberOfFriends ; i++){
			int popularityOfFriendInserted = Integer.parseInt(friendsPopularityArray[i]);
			while(!friendList.isEmpty() && numberOfFriendsDeleted != 0 && friendList.getLast() < popularityOfFriendInserted){
				if(friendList.getLast() < popularityOfFriendInserted){
					friendList.removeLast();
					numberOfFriendsDeleted--;
				}
			}
			friendList.addLast(popularityOfFriendInserted);
		}
    }
    
    public void displayRemainingFriends(){
        while(!friendList.isEmpty()){
        	System.out.print(friendList.removeFirst()+" ");
        }
        System.out.println();
    }
    
    public static void main(String args[] ) throws Exception  {
        RemoveFriends friendRemoval = new RemoveFriends();
        int testCases = Integer.parseInt(friendRemoval.bufferReader.readLine());
        friendRemoval.startRemovalProcess(testCases);
    }
}